package com.cw.mapper;

import com.cw.entity.Bill;
import com.cw.entity.Payway;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @Author:cheng
 * @Date:2023/6/18 10:10
 */
@Mapper
public interface BillMapper {
    int add(Bill bill);

    int update(Bill bill);

    int del(int id);

    List<Bill> findByWhere(PageModel<Bill> model);

    List<Bill> findByWhereNoPage(Bill bill);

    List<Map<String,Float>> getMonthlyInfo(PageModel<Bill> model);

    int getTotalByWhere(PageModel<Bill> model);

    List<Payway> getAllPayways();
}
