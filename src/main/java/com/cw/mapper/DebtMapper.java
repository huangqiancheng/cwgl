package com.cw.mapper;



import com.cw.entity.Bill;
import com.cw.entity.Debt;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DebtMapper {
    int add(Debt debt);

    int update(Debt debt);

    int del(int id);

    List<Debt> findByWhere(PageModel<Debt> model);

    List<Debt> findByWhereNoPage(Debt model);

    int getTotalByWhere(PageModel<Bill> model);

    Debt select(int id);
}
