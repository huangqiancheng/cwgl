package com.cw.mapper;

import com.cw.entity.House;
import com.cw.entity.Role;
import com.cw.entity.User;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 11:15
 */

@Mapper
public interface UserMapper{
    /**
     * 获取单个用户信息，可用于：
     * 1：登录
     * 2：通过用户某一部分信息获取完整用户信息
     * @param user
     * @return
     */
    User getUserInfo(User user);

    /**
     * 判断用户是否登录
     * @param user
     * @return
     */
    int userIsExisted(User user);

    /**
     * 注册用户
     * @param user
     * @return
     */
    int addUser(User user);

    int addHouseId(House house);

    int update(User user);

    List<User> getUsersByWhere(PageModel<User> pageModel);

    int getTotalByWhere(PageModel<User> pageModel );

    List<Role> getAllRoles();

    int delete(String id);

    /**
     * 修改密码
     * @param id
     * @param newPassword
     * @return
     */
    int changePassword(Integer id,String newPassword);
}
