package com.cw.mapper;


import com.cw.entity.Wages;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface WagesMapper {

    int del(int id);

    int add(Wages wages);

    int update(Wages wages);

    List<Wages> findByWhere(PageModel<Wages> model);

    int getTotalByWhere(PageModel model);
}
