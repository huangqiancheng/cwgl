package com.cw.mapper;

import com.cw.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/20 20:03
 */
@Mapper
public interface RoleMapper {
    List<Role> getAllRoles();

    List<Role> getRoles();

    int addRole(Role role);

    int updateRole(Role role);

    int deleteRole(String id);

    Role getRoleById(String id);

    Role selectRoleByName1(@Param("name") String name);

    void insertPrivileges(@Param("roleid") int i, @Param("id") String s);

    void deletePrivilege(@Param("id") String roleid);

    Role selectRoleByName(@Param("name") String name,@Param("id") String roleid);

    List<String> getAllPrivilege(@Param("id") String roleId);
}
