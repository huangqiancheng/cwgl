package com.cw.mapper;




import com.cw.entity.Bill;
import com.cw.entity.MoneyManage;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MoneyManageMapper {

    int add(MoneyManage moneyManage);

    int update(MoneyManage moneyManage);

    int del(int id);

    List<MoneyManage> findByWhere(PageModel<MoneyManage> model);

   List<MoneyManage> findByWhereNoPage(MoneyManage model);

    int getTotalByWhere(PageModel<Bill> model);


}
