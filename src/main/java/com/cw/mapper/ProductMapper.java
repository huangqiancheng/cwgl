package com.cw.mapper;


import com.cw.entity.Product;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {
    List<Product> getProducts(PageModel model);

    Integer getProductsTotal(PageModel model);

    int del(int id);

    int add(Product product);

    List<Product> findByWhereNoPage(Product product);

    int update(Product product);
}
