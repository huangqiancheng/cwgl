package com.cw.mapper;

import com.cw.entity.Card;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/19 15:27
 */
@Mapper
public interface CardMapper {
    int del(int id);

    int add(Card card);

    int update(Card card);

    List<Card> findByWhere(PageModel<Card> model);

    int getTotalByWhere(PageModel model);
}
