package com.cw.mapper;


import com.cw.entity.Insurance;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InsuranceMapper {

    int add(Insurance insurance);

    int del(int id);

    int update(Insurance insurance);

    List<Insurance> findByWhere(PageModel<Insurance> model);

    int getTotalByWhere(PageModel model);
}
