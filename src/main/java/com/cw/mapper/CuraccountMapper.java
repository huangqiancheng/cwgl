package com.cw.mapper;


import com.cw.entity.Bill;
import com.cw.entity.Curaccount;
import com.cw.utils.PageModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CuraccountMapper {
    int add(Curaccount curaccount);

    int update(Curaccount curaccount);

    int del(int id);

    List<Curaccount> findByWhere(PageModel<Curaccount> model);

    List<Curaccount> findByWhereNoPage(Curaccount model);

    int getTotalByWhere(PageModel<Bill> model);

    Curaccount queryOneByBill(Bill bill);

    String getMoney(Integer userid);
}
