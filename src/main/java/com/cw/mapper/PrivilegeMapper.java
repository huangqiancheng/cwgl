package com.cw.mapper;

import com.cw.entity.Privilege;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 15:21
 */
@Mapper
public interface PrivilegeMapper {
    List<Privilege> getPrivilegeByRoleid(int roleid);
}
