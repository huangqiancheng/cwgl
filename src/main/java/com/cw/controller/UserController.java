package com.cw.controller;

import com.cw.entity.Privilege;
import com.cw.entity.Role;
import com.cw.entity.User;
import com.cw.service.PrivilegeService;
import com.cw.service.UserService;
import com.cw.utils.*;
import com.wf.captcha.utils.CaptchaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.cw.utils.Config.CURRENT_USERNAME;

/**
 * @Author:cheng
 * @Date:2023/6/16 11:26
 */
@Slf4j
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PrivilegeService privilegeService;

    @RequestMapping(value = {"/", "login.html"})
    public String toLogin(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        if (session.getAttribute(CURRENT_USERNAME) == null) {
            return "login";
        } else {
            try {
                response.sendRedirect("/pages/index");
            } catch (IOException e) {
                e.printStackTrace();
                return "login";
            }
            return null;
        }
    }

    @RequestMapping(value = "/login.do")
    @ResponseBody
    public Result getUserInfo(User userInfo, HttpServletRequest request, HttpServletResponse response) {
        //清除session中的验证码
        if (!CaptchaUtil.ver(userInfo.getVerity(), request)) {
            CaptchaUtil.clear(request);
            return ResultUtil.unSuccess("验证码错误");
        }
        boolean isExisted = userService.userIsExisted(userInfo);

        log.info(isExisted + "-" + request.getHeader("token"));

        User user = getUserInfo(userInfo);
        if ("client".equals(request.getHeader("token")) || !isExisted) {
            //用户不存在
            return ResultUtil.unSuccess("用户不存在");
        }
        if (isExisted && !user.getPassword().equals(MD5Util.md5(userInfo.getPassword()))) {
            return ResultUtil.unSuccess("用户名或密码错误");
        } else {
            //将用户信息存入Session
            userInfo = setSessionUserInfo(user, request.getSession());
            //将用户信息存入Cookie
            setCookieUser(request, response);
            return ResultUtil.success("登录成功", userInfo);
        }
    }

    private User getUserInfo(User user) {
        return userService.getUserInfo(user);
    }

    private User setSessionUserInfo(User user, HttpSession session) {
        List<Privilege> privileges = privilegeService.getPrivilegeByRoleid(user.getRoleid());
        user.setPrivileges(privileges);
        session.setAttribute(Config.CURRENT_USERNAME, user);
        return user;
    }

    private void setCookieUser(HttpServletRequest request, HttpServletResponse response) {
        User user = getSessionUser(request.getSession());
        Cookie cookie = new Cookie(Config.CURRENT_USERNAME, user.getUsername() + "_" + user.getId());
        cookie.setMaxAge(60 * 60 * 24 * 7);
        response.addCookie(cookie);
    }

    @RequestMapping("/getSessionUser")
    @ResponseBody
    public static User getSessionUser(HttpSession session) {
        User user = (User) session.getAttribute(Config.CURRENT_USERNAME);
        user.setPassword(null);
        return user;
    }


    /**
     * 进入注册页面
     *
     * @return
     */
    @RequestMapping(value = {"/register.html"})
    public String toRegister() {
        return "register";
    }


    /**
     * 注册验证
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/register.do")
    @ResponseBody
    public Result addUserInfo(User userInfo) {
        System.out.println("注册信息：" + userInfo);
        boolean userIsExisted = userService.userIsExisted(userInfo);
        if (userIsExisted) {
            return ResultUtil.unSuccess("用户已经存在");
        }
        //userInfo.setPassword(MD5Util.md5(userInfo.getPassword())); //密码加密.
        if (userService.addUser(userInfo) > 0) {
            return ResultUtil.success("注册成功");
        }
        return ResultUtil.unSuccess("注册失败");
    }

    /**
     * 退出登录,移除session和cookie
     *
     * @param request
     * @param response
     * @return 进入login.html
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        delCookieUser(request, response);
        request.getSession().removeAttribute(Config.CURRENT_USERNAME);
        return "login";
    }


    /**
     * 退出时删除cookie信息
     *
     * @param request
     * @param response
     */
    private void delCookieUser(HttpServletRequest request, HttpServletResponse response) {
        User user = getSessionUser(request.getSession());
        Cookie cookie = new Cookie(Config.CURRENT_USERNAME, user.getUsername() + "_" + user.getId());
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }


    /**
     * 获取用户信息
     * @param user
     * @param pageNo
     * @param pageSize
     * @param role
     * @param session
     * @return
     */
    @RequestMapping("/users/getUsersByWhere/{pageNo}/{pageSize}")
    public @ResponseBody
    Result getUsersByWhere(User user, @PathVariable int pageNo,
                           @PathVariable int pageSize, int role, HttpSession session) {
        if (1 == role) {
            user.setHouseid("");
        }
        PageModel model = new PageModel<>(pageNo, user);
        model.setPageSize(pageSize);
        return userService.getUsersByWhere(model);
    }

    /**
     * 更新用户数据
     *
     * @param userInfo
     * @return
     */
    @RequestMapping("/user/update")
    @ResponseBody
    public Result updateUser(User userInfo) {
        try {
            int num = userService.update(userInfo);
            if (num > 0) {
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @RequestMapping("/user/del/{id}")
    @ResponseBody
    public Result deleteUser(@PathVariable String id) {
        System.out.println("获取id+" + id);
        try {
            int num = userService.delete(id);
            if (num > 0) {
                return ResultUtil.success();
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }


    /**
     * 用户修改密码
     *
     * @param userInfo
     * @param newPassword
     * @param reNewPassword
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/user/password/change")
    @ResponseBody
    public Result updateUserPassword(User userInfo, @RequestParam(name = "newPassword") String newPassword, @RequestParam(name = "reNewPassword") String reNewPassword, HttpServletRequest request, HttpServletResponse response) {
        if (!reNewPassword.equals(newPassword)) {
            return ResultUtil.unSuccess("两次新密码不一致！");
        }
        String old_password = userInfo.getPassword();
        User user = getUserInfo(userInfo);
        if (!userService.userIsExisted(userInfo)) {
            return ResultUtil.unSuccess("用户不存在！");
        }
        if (!MD5Util.md5(old_password).equals(user.getPassword())) {
            return ResultUtil.unSuccess("原密码错误！");
        }
        try {
            user.setPassword(MD5Util.md5(newPassword));
            int num = userService.changePassword(userInfo, user.getPassword());
            if (num > 0) {
                delCookieUser(request, response);
                request.getSession().removeAttribute(Config.CURRENT_USERNAME);
                //System.out.println(request.getSession().getAttribute(Config.CURRENT_USERNAME));
                return ResultUtil.success("修改成功");
            } else {
                return ResultUtil.unSuccess("失败");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

}
