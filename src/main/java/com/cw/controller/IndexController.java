package com.cw.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author:cheng
 * @Date:2023/6/16 17:26
 */
@Controller
public class IndexController {

    @RequestMapping("/pages/{page}")  //为了实现动态跳转
    public String toPage(@PathVariable String page){
        return page.replace("_","/");
    }
}
