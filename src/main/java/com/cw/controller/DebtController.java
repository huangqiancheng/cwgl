package com.cw.controller;


import com.cw.entity.Bill;
import com.cw.entity.Debt;
import com.cw.entity.User;
import com.cw.service.BillService;
import com.cw.service.DebtService;
import com.cw.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/debt")
public class DebtController {

    @Resource
    private DebtService debtService;

    @Autowired
    private BillService billService;


    @RequestMapping("/getDebt/{pageNo}/{pageSize}")
    public Result<Debt> getBillsByWhere(Debt debt, @PathVariable int pageNo, @PathVariable int pageSize, HttpSession session){
        debt = getHouseBill(debt,session);
        PageModel model = new PageModel<>(pageNo,debt);
        model.setPageSize(pageSize);
        return debtService.findByWhere(model);
    }


    private Debt getHouseBill(Debt debt, HttpSession session) {
        User currentUser = Config.getSessionUser(session);
        //当登录用户为家主时，查询默认查询全家账单情况
        //当登录用户为普通用户时，仅查询当前用户的账单
        if (currentUser.getRoleid() == 2){
            debt.setHouseid(currentUser.getHouseid());
        }else if (currentUser.getRoleid() == 3){
            debt.setUserid(currentUser.getId());
        }
        return debt;
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Result add(Debt debt, HttpSession session){
        if (Config.getSessionUser(session)!=null){
            debt.setUserid(Config.getSessionUser(session).getId());
            debt.setHouseid(Config.getSessionUser(session).getHouseid());
        }
        Utils.log(debt.toString());
        try {
            int num = debtService.add(debt);
            if(num>0){
                int billid = debt.getId();
                debt = new Debt();
                debt.setId(billid);
                return ResultUtil.success("记录成功！",debtService.findByWhereNoPage(debt));
            }else {
                return ResultUtil.unSuccess();
            }
        }catch (Exception e){
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/update")
    public Result update(Debt debt, HttpSession session){
        if (Config.getSessionUser(session)!=null){
            debt.setUserid(Config.getSessionUser(session).getId());
            debt.setHouseid(Config.getSessionUser(session).getHouseid());
        }
        Utils.log(debt.toString());
        int num = debtService.update(debt);
        if(num>0){
            return ResultUtil.success("修改成功！",null);
        }else {
            return ResultUtil.unSuccess();
        }
    }

    @RequestMapping("/del")
    public Result del(int id){
        try {
            int num = debtService.del(id);
            if(num>0){
                return ResultUtil.success("删除成功！",null);
            }else {
                return ResultUtil.unSuccess();
            }
        }catch (Exception e){
            return ResultUtil.error(e);
        }
    }


    @RequestMapping("/repayment")
    public Result repayment(int id,HttpSession session){
        try {
            Debt debt = debtService.select(id);
            if (debt != null){
                Bill bill = new Bill();
                bill.setTitle(debt.getName());
                bill.setTypeid(1);
                bill.setMoney(debt.getCurperiod());
                bill.setPaywayid(5);
                bill.setRemark("还款");
                if (Config.getSessionUser(session)!=null){
                    bill.setUserid(Config.getSessionUser(session).getId());
                    bill.setHouseid(Config.getSessionUser(session).getHouseid());
                }
                billService.add(bill);
                debt.setResidue(debt.getResidue().subtract(debt.getCurperiod()));
                debtService.update(debt);
                return ResultUtil.success("还款成功！",null);
            }else {
                return ResultUtil.unSuccess();
            }
        }catch (Exception e){
            return ResultUtil.error(e);
        }
    }

}

