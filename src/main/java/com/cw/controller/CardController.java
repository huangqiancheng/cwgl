package com.cw.controller;


import com.cw.entity.Card;
import com.cw.entity.CardType;
import com.cw.entity.User;
import com.cw.service.CardService;
import com.cw.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/card")
public class CardController {

    @Autowired
    private CardService cardService;

    @RequestMapping("/getCards/{pageNo}/{pageSize}/{type}")
    public Result<Card> getCards(@PathVariable String type, @PathVariable int pageNo, @PathVariable int pageSize, HttpSession session) {
        Card card = new Card();
        if ("-1".equals(type)) {
            card.setType(null);
        } else {
            card.setType(type);
        }
        card = getHouseCard(card, session);
        PageModel model = new PageModel<>(pageNo, card);
        model.setPageSize(pageSize);
        return cardService.getCards(model);
    }


    private Card getHouseCard(Card card, HttpSession session) {
        User currentUser = Config.getSessionUser(session);
        //当登录用户为家主时，查询默认查询全家账单情况
        //当登录用户为普通用户时，仅查询当前用户的账单
        if (currentUser.getRoleid() == 2) {
            card.setHouseid(currentUser.getHouseid());
        } else if (currentUser.getRoleid() == 3) {
            card.setUserid(currentUser.getId());
        }
        return card;
    }

    @RequestMapping("/getCardType")
    public Result<CardType> getCardType() {
        List<CardType> list = new ArrayList<>();
        list.add(new CardType("R1", "银行卡"));
        list.add(new CardType("R2", "超市卡"));
        list.add(new CardType("R3", "会员卡"));
        list.add(new CardType("R4", "公交卡"));
        list.add(new CardType("R5", "其他"));
        return ResultUtil.success(list);
    }

    @RequestMapping(value = "/addCard", method = RequestMethod.POST)
    public Result addCard(Card card, HttpSession session) {
        try {
            if (Config.getSessionUser(session) != null) {
                card.setUserid(Config.getSessionUser(session).getId());
            }
            int result = cardService.addCard(card);
            if (result > 0) {
                return ResultUtil.success("新增成功！");
            }
            return ResultUtil.unSuccess("失败");
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping(value = "/updateCard" ,method = RequestMethod.POST)
    public Result updateCard(Card card ,HttpSession session) {
        //Utils.log(card.toString());
        try {
            int result = cardService.updateCard(card);
            if (result > 0) {
                return ResultUtil.success("修改成功！", null);
            }
            return ResultUtil.unSuccess();
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping(value = "/delCard", method = RequestMethod.POST)
    public Result delCard(int id) {
        try {
            cardService.delCard(id);
            return ResultUtil.success("删除成功！", null);
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }
}
