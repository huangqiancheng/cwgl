package com.cw.controller;


import com.cw.entity.Insurance;
import com.cw.entity.User;
import com.cw.service.InsuranceService;
import com.cw.utils.Config;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/insurance")
public class InsuranceController {

    @Autowired
    private InsuranceService insuranceService;

    @RequestMapping("/getInsurance/{pageNo}/{pageSize}")
    public Result<Insurance> getInsurance(Insurance insurance, @PathVariable int pageNo, @PathVariable int pageSize, HttpSession session) {
        insurance = getHouseInsurance(insurance, session);
        PageModel model = new PageModel<>(pageNo, insurance);
        model.setPageSize(pageSize);
        return insuranceService.getInsurance(model);
    }


    private Insurance getHouseInsurance(Insurance insurance, HttpSession session) {
        User currentUser = Config.getSessionUser(session);
        //当登录用户为家主时，查询默认查询全家账单情况
        //当登录用户为普通用户时，仅查询当前用户的账单
        if (currentUser.getRoleid() == 2) {
            insurance.setHouseid(currentUser.getHouseid());
        } else if (currentUser.getRoleid() == 3) {
            insurance.setUserid(currentUser.getId());
        }
        return insurance;
    }


    @RequestMapping(value = "/addInsurance", method = RequestMethod.POST)
    public Result addInsurance(Insurance insurance, HttpSession session) {
        try {
            if (Config.getSessionUser(session)!=null){
                insurance.setUserid(Config.getSessionUser(session).getId());
            }
            int num = insuranceService.addInsurance(insurance);
            if (num > 0) {
                return ResultUtil.success("新增成功！");
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/updateInsurance")
    public Result updateInsurance(Insurance insurance, HttpSession session) {
        try {
            int num = insuranceService.updateInsurance(insurance);
            if (num > 0) {
                return ResultUtil.success("修改成功！", null);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/delInsurance")
    public Result delCard(int id) {
        try {
            insuranceService.delInsurance(id);
            return ResultUtil.success("删除成功！", null);
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }
}
