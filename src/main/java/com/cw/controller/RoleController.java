package com.cw.controller;

import com.cw.entity.Role;
import com.cw.entity.RoleVo;
import com.cw.mapper.RoleMapper;
import com.cw.service.RoleService;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/20 20:44
 */
@Controller
public class RoleController {
    @Autowired
    private RoleService roleService ;

    @Autowired
    private RoleMapper roleMapper ;


    /**
     * 获取所有角色信息
     *
     * @return
     */
    @RequestMapping("/getAllRoles")
    @ResponseBody
    public Result<Role> getAllRoles() {
        try {
            List<Role> roles = roleService.getAllRoles();
            if (roles.size() > 0) {
                return ResultUtil.success(roles);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/getRoles")
    @ResponseBody
    public Result<Role> getRoles() {
        try {
            List<Role> roles = roleService.getRoles();
            if (roles.size() > 0) {
                return ResultUtil.success(roles);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @RequestMapping("/role/add")
    @ResponseBody
    @Transactional
    public Result addRole(RoleVo role) {
        Role role1 = new Role();
        role1.setRolename(role.getName());
        Role role2 = roleService.selectRoleByName1(role.getName());
        if (role2 != null) {
            return ResultUtil.unSuccess("该角色已存在");
        }
        roleService.addRole(role1);
        String[] id = role.getId();
        if (id != null) {
            id = deleteArrayNull(id);
        }
        if (id != null && id.length > 0) {
            String join = String.join(",", id);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            String[] split = join.split(",");
            for (int i1 = 0; i1 < split.length; i1++) {
                roleMapper.insertPrivileges(role1.getRoleid(), split[i1]);
            }
        }
        return ResultUtil.success();
    }


    /**
     * 更新角色信息
     *
     * @param role
     * @return
     */
    @RequestMapping("/role/update")
    @ResponseBody
    public Result updateRole(RoleVo role) {
        roleMapper.deletePrivilege(role.getRoleid());
        Role role2 = roleMapper.selectRoleByName(role.getName(), role.getRoleid());
        if (role2 != null) {
            return ResultUtil.unSuccess("该角色已存在");
        }
        Role role1 = new Role();
        role1.setRoleid(Integer.valueOf(role.getRoleid()));
        role1.setRolename(role.getName());
        roleService.updateRole(role1);
        String[] id = role.getId();
        if (id != null) {
            id = deleteArrayNull(id);
        }
        if (id != null && id.length > 0) {
            String join = String.join(",", id);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            String[] split = join.split(",");
            for (int i1 = 0; i1 < split.length; i1++) {
                roleMapper.insertPrivileges(role1.getRoleid(), split[i1]);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 删除角色信息
     *
     * @param roleid
     * @return
     */
    @RequestMapping("/role/del/{roleid}")
    @ResponseBody
    public Result deleteRole(@PathVariable String roleid) {
        roleMapper.deletePrivilege(roleid);
        roleService.deleteRole(roleid);
        return ResultUtil.success();
    }

    @RequestMapping("/getRole/{id}")
    @ResponseBody
    public Result getRoleById(@PathVariable String id) {
        try {
            Role role = roleService.getRoleById(id);
            if (role != null) {
                return ResultUtil.success(role);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/getAllPrivilege")
    @ResponseBody
    public String getAllPrivilege(String roleId) {
        List<String> ids = roleService.getAllPrivilege(roleId);
        if (!CollectionUtils.isEmpty(ids)) {
            String join = String.join(",", ids);
            if (join.indexOf("65") > 0 || join.indexOf("63") > 0) {
                join += ",62";
            }
            if (join.indexOf("75") > 0 || join.indexOf("76") > 0 || join.indexOf("77") > 0 || join.indexOf("78") > 0 || join.indexOf("79") > 0 || join.indexOf("80") > 0 || join.indexOf("81") > 0) {
                join += ",64";
            }
            if (join.indexOf("67") > 0) {
                join += ",66";
            }
            if (join.indexOf("74") > 0) {
                join += ",68";
            }
            if (join.indexOf("70") > 0 || join.indexOf("71") > 0) {
                join += ",69";
            }
            return join;
        }
        return "";
    }


    /***
     * 去除String数组中的空值
     */
    private String[] deleteArrayNull(String string[]) {
        String strArr[] = string;

        // step1: 定义一个list列表，并循环赋值
        ArrayList<String> strList = new ArrayList<String>();
        for (int i = 0; i < strArr.length; i++) {
            strList.add(strArr[i]);
        }

        // step2: 删除list列表中所有的空值
        while (strList.remove(null)) ;
        while (strList.remove("")) ;

        // step3: 把list列表转换给一个新定义的中间数组，并赋值给它
        String strArrLast[] = strList.toArray(new String[strList.size()]);

        return strArrLast;
    }
}
