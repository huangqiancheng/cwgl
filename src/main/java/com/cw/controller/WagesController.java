package com.cw.controller;


import com.cw.entity.Bill;
import com.cw.entity.User;
import com.cw.entity.Wages;
import com.cw.service.BillService;
import com.cw.service.WagesService;
import com.cw.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/wages")
public class WagesController {

    @Autowired
    private WagesService wagesService;

    @Autowired
    private BillService billService;

    @RequestMapping("/getWages/{pageNo}/{pageSize}")
    public Result<Wages> getWages(@PathVariable int pageNo, @PathVariable int pageSize, HttpSession session) {
        Wages wages = new Wages();
        wages = getHouseWages(wages, session);
        PageModel model = new PageModel<>(pageNo, wages);
        model.setPageSize(pageSize);
        return wagesService.getWages(model);
    }


    private Wages getHouseWages(Wages wages, HttpSession session) {
        User currentUser = Config.getSessionUser(session);
        //当登录用户为家主时，查询默认查询全家账单情况
        //当登录用户为普通用户时，仅查询当前用户的账单
        if (currentUser.getRoleid() == 2) {
            wages.setHouseid(currentUser.getHouseid());
        } else if (currentUser.getRoleid() == 3) {
            wages.setUserid(currentUser.getId());
        }
        return wages;
    }



    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Result add(Wages wages, HttpSession session) {
        try {
            if (Config.getSessionUser(session)!=null){
                wages.setUserid(Config.getSessionUser(session).getId());
            }
            int num = wagesService.add(wages);
            Bill bill = new Bill();
            bill.setTitle("工资");
            bill.setTypeid(2);
            bill.setMoney(wages.getSumwage());
            bill.setPaywayid(5);
            bill.setRemark("工资录入");
            if (Config.getSessionUser(session)!=null){
                bill.setUserid(Config.getSessionUser(session).getId());
                bill.setHouseid(Config.getSessionUser(session).getHouseid());
            }
            billService.add(bill);
            if (num > 0) {
                return ResultUtil.success("录入成功！");
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/update")
    public Result update(Wages wages, HttpSession session) {
        Utils.log(wages.toString());
        try {
            int num = wagesService.update(wages);
            if (num > 0) {
                return ResultUtil.success("修改成功！", null);
            } else {
                return ResultUtil.unSuccess();
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @RequestMapping("/del")
    public Result del(int id) {
        try {
            wagesService.del(id);
            return ResultUtil.success("删除成功！", null);
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }
}
