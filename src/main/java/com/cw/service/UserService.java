package com.cw.service;

import com.cw.entity.Role;
import com.cw.entity.User;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 11:19
 */

public interface UserService{
    User getUserInfo(User user);

    boolean userIsExisted(User user);

    int addUser(User user);

    int update(User user);

    Result<User> getUsersByWhere(PageModel<User> pageModel);

    List<Role> getAllRoles();

    int delete(String id);

    int changePassword(User userInfo,String newPassword);
}
