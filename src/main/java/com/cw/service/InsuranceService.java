package com.cw.service;


import com.cw.entity.Insurance;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

public interface InsuranceService {
    Result<Insurance> getInsurance(PageModel model);

    int addInsurance(Insurance insurance);


    int updateInsurance(Insurance insurance);

    void delInsurance(int id);
}
