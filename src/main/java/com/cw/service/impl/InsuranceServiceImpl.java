package com.cw.service.impl;


import com.cw.entity.Insurance;
import com.cw.mapper.InsuranceMapper;
import com.cw.service.InsuranceService;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InsuranceServiceImpl implements InsuranceService {

    @Autowired
    private InsuranceMapper insuranceMapper;

    @Override
    public Result<Insurance> getInsurance(PageModel model) {
        try {
            List<Insurance> insuranceList = insuranceMapper.findByWhere(model);
            if (insuranceList.size() >= 0) {
                Result<Insurance> result = ResultUtil.success(insuranceList);
                result.setTotal(insuranceMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int addInsurance(Insurance insurance) {
        return insuranceMapper.add(insurance);
    }

    @Override
    public int updateInsurance(Insurance insurance) {
        return insuranceMapper.update(insurance);
    }

    @Override
    public void delInsurance(int id) {
        insuranceMapper.del(id);
    }
}
