package com.cw.service.impl;

import com.cw.entity.Privilege;
import com.cw.mapper.PrivilegeMapper;
import com.cw.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 15:24
 */

@Service
public class PrivilegeServiceImpl implements PrivilegeService {
    @Autowired
    private PrivilegeMapper privilegeMapper;

    @Override
    public List<Privilege> getPrivilegeByRoleid(int roleid) {
        return privilegeMapper.getPrivilegeByRoleid(roleid);
    }
}
