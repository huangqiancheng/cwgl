package com.cw.service.impl;

import com.cw.entity.Role;
import com.cw.mapper.RoleMapper;
import com.cw.service.RoleService;
import com.cw.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/20 20:41
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> getAllRoles() {
        return roleMapper.getAllRoles();
    }

    @Override
    public List<Role> getRoles() {
        return roleMapper .getRoles() ;
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.addRole(role);
    }

    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    @Override
    public int deleteRole(String id) {
        return roleMapper.deleteRole(id);
    }

    @Override
    public Role getRoleById(String id) {
        return roleMapper.getRoleById(id);
    }

    @Override
    public Role selectRoleByName1(String name) {
        return roleMapper.selectRoleByName1(name );
    }

    @Override
    public List<String> getAllPrivilege(String roleId) {
        return roleMapper.getAllPrivilege(roleId );
    }
}
