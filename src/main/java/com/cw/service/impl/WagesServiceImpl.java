package com.cw.service.impl;


import com.cw.entity.Wages;
import com.cw.mapper.WagesMapper;
import com.cw.service.WagesService;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WagesServiceImpl implements WagesService {

    @Autowired
    private WagesMapper wagesMapper;


    @Override
    public Result<Wages> getWages(PageModel model) {
        try {
            List<Wages> wagesList = wagesMapper.findByWhere(model);
            if (wagesList.size() >= 0) {
                Result<Wages> result = ResultUtil.success(wagesList);
                result.setTotal(wagesMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int add(Wages wages) {
        return wagesMapper.add(wages);
    }

    @Override
    public int update(Wages wages) {
        return wagesMapper.update(wages);
    }

    @Override
    public void del(int id) {
        wagesMapper.del(id);
    }
}
