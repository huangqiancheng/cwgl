package com.cw.service.impl;


import com.cw.entity.MoneyManage;
import com.cw.mapper.MoneyManageMapper;
import com.cw.service.MoneyManageService;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyServiceImpl implements MoneyManageService {
    @Autowired
    private MoneyManageMapper moneyManageMapper;
    @Override
    public Result<MoneyManage> findByWhere(PageModel model) {

            List<MoneyManage> bills = moneyManageMapper.findByWhere(model);
            if (bills.size()>=0){
                Result<MoneyManage> result = ResultUtil.success(bills);
                result.setTotal(moneyManageMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            }else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
    }

    @Override
    public int add(MoneyManage moneyManage) {
        return moneyManageMapper.add(moneyManage);
    }

    @Override
    public int update(MoneyManage moneyManage) {
        return moneyManageMapper.update(moneyManage);
    }

    @Override
    public int del(int id) {
        return moneyManageMapper.del(id);
    }

    @Override
    public Result<MoneyManage> findByWhereNoPage(MoneyManage moneyManage) {
        List<MoneyManage> bills = moneyManageMapper.findByWhereNoPage(moneyManage);
        if (bills.size()>=0){
            Result<MoneyManage> result = ResultUtil.success(bills);
            result.setMsg("数据获取成功");
            return result;
        }else {
            return ResultUtil.unSuccess("没有找到符合条件的属性！");
        }
    }
}
