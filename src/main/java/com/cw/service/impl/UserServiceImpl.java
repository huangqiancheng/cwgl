package com.cw.service.impl;


import com.cw.entity.House;
import com.cw.entity.Role;
import com.cw.entity.User;
import com.cw.mapper.UserMapper;
import com.cw.service.UserService;
import com.cw.utils.MD5Util;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 11:21
 */

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserInfo(User user) {
        return userMapper.getUserInfo(user);
    }

    @Override
    public boolean userIsExisted(User user) {
        return userMapper.userIsExisted(user) > 0 ? true : false;
    }

    @Override
    @Transactional
    public int addUser(User user) {
        //对密码进行MD5加密
        user.setPassword(MD5Util.md5(user.getPassword()));
        int result = userMapper.addUser(user);
        if (user.getRoleid() == 2) {
            //如果是家庭管理员，则新增家庭
            House newHouse = new House();
            newHouse.setOwnerid(user.getId());
            int r = userMapper.addHouseId(newHouse);
            //添加家庭成功后，绑定到该用户
            if (r == 1) {
                user.setHouseid(newHouse.getId().toString());
                result = userMapper.update(user);
            }
        }
        return result;
    }


    @Override
    public int update(User user) {
        //对密码进行MD5加密
        user.setPassword(MD5Util.md5(user.getPassword()));
        return userMapper.update(user);
    }

    @Override
    public Result<User> getUsersByWhere(PageModel<User> pageModel) {
        try {
            List<User> users = userMapper.getUsersByWhere(pageModel);
            if (users.size() >= 0) {
                Result<User> result = ResultUtil.success(users);
                result.setTotal(userMapper.getTotalByWhere(pageModel));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("没有找到符合条件的属性！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }

    }

    @Override
    public List<Role> getAllRoles() {
        return userMapper.getAllRoles();
    }

    //删除用户
    @Override
    public int delete(String id) {
        return userMapper.delete(id);
    }

    @Override
    public int changePassword(User userInfo, String newPassword) {
        return userMapper.changePassword(userInfo.getId(), newPassword);
    }
}
