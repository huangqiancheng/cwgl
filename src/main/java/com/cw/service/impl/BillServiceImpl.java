package com.cw.service.impl;

import com.cw.entity.Bill;
import com.cw.entity.Curaccount;
import com.cw.entity.Payway;
import com.cw.entity.Product;
import com.cw.mapper.BillMapper;
import com.cw.mapper.CuraccountMapper;
import com.cw.mapper.ProductMapper;
import com.cw.service.BillService;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author:cheng
 * @Date:2023/6/18 10:11
 */
@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private BillMapper billMapper;
    @Autowired
    private CuraccountMapper curaccountMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public int add(Bill bill) {
        //根据userid 和 houseid 判断总资产表中是否有记录
        Curaccount curaccount = curaccountMapper.queryOneByBill(bill);
        //有的话根据支出和收入进行操作
        if (curaccount != null) {
            if (bill.getTypeid() == 1)//支出
            {
                curaccount.setMoney(curaccount.getMoney().subtract(bill.getMoney()));
            } else {
                curaccount.setMoney(curaccount.getMoney().add(bill.getMoney()));
            }
            curaccountMapper.update(curaccount);
        } else {
            //没有的话初始化一条数据
            Curaccount curaccount1 = new Curaccount();
            curaccount1.setHouseid(bill.getHouseid());
            curaccount1.setUserid(bill.getUserid());
            curaccount1.setName("家庭资产");
            curaccount1.setRemark("无");
            if (bill.getTypeid() == 1)//支出
            {
                curaccount1.setMoney(new BigDecimal(0).subtract(bill.getMoney()));
            } else {
                curaccount1.setMoney(new BigDecimal(0).add(bill.getMoney()));
            }
            curaccountMapper.add(curaccount1);
        }
        return billMapper.add(bill);
    }

    @Override
    public int update(Bill bill) {
        return billMapper.update(bill);
    }

    @Override
    public int del(int id) {
        return billMapper.del(id);
    }

    @Override
    public Result<Bill> findByWhere(PageModel model) {
        try {
            List<Bill> bills = billMapper.findByWhere(model);
            if (bills.size() >= 0) {
                Result<Bill> result = ResultUtil.success(bills);
                result.setTotal(billMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public Result<Bill> findByWhereNoPage(Bill bill) {
        try {
            List<Bill> bills = billMapper.findByWhereNoPage(bill);
            if (bills.size() >= 0) {
                Result result = ResultUtil.success(bills);
                result.setMsg("数据获取成功");
                return result;
            } else {
                return ResultUtil.unSuccess("没有找到符合条件的属性！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public List<Map<String, Float>> getMonthlyInfo(PageModel<Bill> model) {
        return billMapper.getMonthlyInfo(model);
    }

    @Override
    public List<Payway> getAllPayways() {
        return billMapper.getAllPayways();
    }

    @Override
    public Result<Product> getProducts(PageModel model) {
        try {
            List<Product> products = productMapper.getProducts(model);
            if (products.size() >= 0) {
                Result<Product> result = ResultUtil.success(products);
                result.setTotal(productMapper.getProductsTotal(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int delProduct(int id) {
        return productMapper.del(id);
    }

    @Override
    public int addProduct(Product product) {
        return productMapper.add(product);
    }

    @Override
    public Result<Product> findProductfPage(Product product) {
        try {
            List<Product> bills = productMapper.findByWhereNoPage(product);
            if (bills.size() >= 0) {
                Result<Product> result = ResultUtil.success(bills);
                result.setMsg("数据获取成功");
                return result;
            } else {
                return ResultUtil.unSuccess("没有找到符合条件的属性！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }

    @Override
    public int updateProduct(Product product) {
        return productMapper.update(product);
    }
}
