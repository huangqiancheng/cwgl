package com.cw.service.impl;

import com.cw.entity.Card;
import com.cw.mapper.CardMapper;
import com.cw.service.CardService;
import com.cw.utils.PageModel;
import com.cw.utils.Result;
import com.cw.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/19 15:34
 */
@Service
public class CardServiceImpl implements CardService {
    @Autowired
    private CardMapper cardMapper;

    @Override
    public void delCard(int id) {
        cardMapper.del(id);
    }

    @Override
    public int addCard(Card card) {
        return cardMapper.add(card);
    }

    @Override
    public int updateCard(Card card) {
        return cardMapper.update(card);
    }

    @Override
    public Result<Card> getCards(PageModel<Card> model) {
        try {
            List<Card> cardList = cardMapper.findByWhere(model);
            if (cardList.size() >= 0) {
                Result<Card> result = ResultUtil.success(cardList);
                result.setTotal(cardMapper.getTotalByWhere(model));
                if (result.getTotal() == 0) {
                    result.setMsg("没有查到相关数据");
                } else {
                    result.setMsg("数据获取成功");
                }
                return result;
            } else {
                return ResultUtil.unSuccess("获取数据失败！");
            }
        } catch (Exception e) {
            return ResultUtil.error(e);
        }
    }
}
