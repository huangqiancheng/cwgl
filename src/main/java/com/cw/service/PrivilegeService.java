package com.cw.service;

import com.cw.entity.Privilege;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/16 15:23
 */
public interface PrivilegeService {
    List<Privilege> getPrivilegeByRoleid(int roleid);
}
