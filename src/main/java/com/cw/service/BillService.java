package com.cw.service;

import com.cw.entity.Bill;
import com.cw.entity.Payway;
import com.cw.entity.Product;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

import java.util.List;
import java.util.Map;

/**
 * @Author:cheng
 * @Date:2023/6/18 10:10
 */
public interface BillService {
    int add(Bill bill);

    int update(Bill bill);

    int del(int id);

    Result<Bill> findByWhere(PageModel model);

    Result<Bill> findByWhereNoPage(Bill bill);

    List<Payway> getAllPayways();

    List<Map<String,Float>>  getMonthlyInfo(PageModel<Bill> model);

    Result<Product> getProducts(PageModel model);

    int delProduct(int id);

    int addProduct(Product product);

    Result<Product>  findProductfPage(Product product);

    int updateProduct(Product product);
}
