package com.cw.service;


import com.cw.entity.Debt;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

public interface DebtService {

    Result<Debt> findByWhere(PageModel model);

    int add(Debt debt);

    int update(Debt debt);

    int del(int id);

    Debt select(int id);

    Result<Debt> findByWhereNoPage(Debt debt);
}
