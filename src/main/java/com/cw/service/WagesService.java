package com.cw.service;


import com.cw.entity.Wages;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

public interface WagesService {

    Result<Wages> getWages(PageModel model);

    int add(Wages wages);

    int update(Wages wages);

    void del(int id);
}
