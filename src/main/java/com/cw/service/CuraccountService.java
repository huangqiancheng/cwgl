package com.cw.service;


import com.cw.entity.Curaccount;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

public interface CuraccountService {
    Result<Curaccount> findByWhere(PageModel model);

    int add(Curaccount curaccount);

    int update(Curaccount curaccount);

    int del(int id);

    Result<Curaccount> findByWhereNoPage(Curaccount curaccount);

    String getMoney(Integer userid);
}
