package com.cw.service;

import com.cw.entity.Card;
import com.cw.utils.PageModel;
import com.cw.utils.Result;


/**
 * @Author:cheng
 * @Date:2023/6/19 15:32
 */
public interface CardService {
    void delCard(int id);

    int addCard(Card card);

    int updateCard(Card card);

    Result<Card> getCards(PageModel<Card> model);
}
