package com.cw.service;


import com.cw.entity.MoneyManage;
import com.cw.utils.PageModel;
import com.cw.utils.Result;

public interface MoneyManageService {

    Result<MoneyManage> findByWhere(PageModel model);

    int add(MoneyManage moneyManage);

    int update(MoneyManage moneyManage);

    int del(int id);

    Result<MoneyManage> findByWhereNoPage(MoneyManage moneyManage);
}
