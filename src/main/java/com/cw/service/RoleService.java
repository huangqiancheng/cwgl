package com.cw.service;

import com.cw.entity.Role;
import com.cw.utils.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author:cheng
 * @Date:2023/6/20 20:39
 */
public interface RoleService {
    List<Role> getAllRoles();

    List<Role> getRoles();

    int addRole(Role role);

    int updateRole(Role role);

    int deleteRole(String id);

    Role getRoleById(String id);

    Role selectRoleByName1(@Param("name") String name);

    List<String> getAllPrivilege(String roleId);
}
