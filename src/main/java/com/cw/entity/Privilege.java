package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Privilege {
    private Integer ID;
    private String privilegeNumber;
    private String privilegeName;
    private String privilegeTipflag;
    private String privilegeTypeflag;
    private String privilegeUrl;
    private String icon;


}


