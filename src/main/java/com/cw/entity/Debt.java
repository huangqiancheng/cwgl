package com.cw.entity;

   import java.math.BigDecimal;

   public class Debt
   {
     private Integer id;
     private Integer userid;
     private String houseid;
     private String name;
     private BigDecimal residue;
     private BigDecimal curperiod;
     private String realname;

     public Integer getId() {
/* 16 */     return this.id;
     }

     public void setId(Integer id) {
/* 20 */     this.id = id;
     }

     public Integer getUserid() {
/* 24 */     return this.userid;
     }

     public void setUserid(Integer userid) {
/* 28 */     this.userid = userid;
     }

     public String getHouseid() {
/* 32 */     return this.houseid;
     }

     public void setHouseid(String houseid) {
/* 36 */     this.houseid = houseid;
     }

     public String getName() {
/* 40 */     return this.name;
     }

     public void setName(String name) {
/* 44 */     this.name = name;
     }

     public BigDecimal getResidue() {
/* 48 */     return this.residue;
     }

     public void setResidue(BigDecimal residue) {
/* 52 */     this.residue = residue;
     }

     public BigDecimal getCurperiod() {
/* 56 */     return this.curperiod;
     }

     public void setCurperiod(BigDecimal curperiod) {
/* 60 */     this.curperiod = curperiod;
     }

     public String getRealname() {
/* 64 */     return this.realname;
     }

     public void setRealname(String realname) {
/* 68 */     this.realname = realname;
     }

     public Debt(Integer id, Integer userid, String houseid, String name, BigDecimal residue, BigDecimal curperiod) {
/* 72 */     this.id = id;
/* 73 */     this.userid = userid;
/* 74 */     this.houseid = houseid;
/* 75 */     this.name = name;
/* 76 */     this.residue = residue;
/* 77 */     this.curperiod = curperiod;
     }

     public Debt() {}
   }


/* Location:              D:\ffms\webapps\ROOT\WEB-INF\classes\!\com\xy\ffms\entity\Debt.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */
