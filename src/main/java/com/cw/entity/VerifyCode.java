package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VerifyCode {
    private String code;
    private byte[] imgBytes;
    private long expireTime;


}

