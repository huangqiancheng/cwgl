package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Card {
    private Integer id;
    private String cardname;
    private String type;
    private Integer userid;
    private String typename;
    private String cardnum;
    private String createtime;
    private String houseid;
    private String realname;

}


