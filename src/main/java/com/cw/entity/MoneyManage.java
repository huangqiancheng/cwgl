package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyManage {
    private Integer id;
    private Float money;
    private Integer userid;
    private String name;
    private String rate;
    private String houseid;
    private String realname;


}


