package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payway {
    private Integer id;
    private String payway;
    private String extra;
    private Integer userid;
    private String houseid;
    private String realname;


}

