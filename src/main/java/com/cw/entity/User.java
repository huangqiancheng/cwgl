package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String username;
    private String password;
    private String realname;
    private Integer roleid;
    private String rolename;
    private String houseid;
    private String photo;
    private String signature;
    private String verity;
    private List<Privilege> privileges;

    public void setUsername(String username) {
        if ("".equals(username.trim())) return;
        this.username = username;
    }

    public void setRealname(String realname) {
        if ("".equals(realname.trim())) return;
        this.realname = realname;
    }

    public void setHouseid(String houseid) {
        if (houseid == null || houseid.length() == 0) {
            this.houseid = null;
        } else {
            this.houseid = houseid;
        }
    }
}

