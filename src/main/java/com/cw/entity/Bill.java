package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bill {
    private Integer id;
    private String title;
    private String type;
    private Integer typeid;
    private BigDecimal money;
    private Integer userid;
    private String realname;
    private String remark;
    private Integer paywayid;
    private String payway;
    private String time;
    private String startTime;
    private String endTime;
    private String houseid;

    public void setTitle(String title) {
        if("".equals(title.trim())) return;
        this.title = title;
    }

    public void setRemark(String remark) {
        if("".equals(remark.trim())) return;
        this.remark = remark;
    }

    public void setRealname(String realname) {
        if("".equals(realname.trim())) return;
        this.realname = realname;
    }

    public void setStartTime(String startTime) {
        if("".equals(startTime.trim())) return;
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        if("".equals(endTime.trim())) return;;
        this.endTime = endTime;
    }
}

