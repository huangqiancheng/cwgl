package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Insurance {
    private Integer id;
    private String insurancename;
    private String insuranceno;
    private Integer userid;
    private String start;
    private String end;
    private String createtime;
    private String houseid;
    private String realname;


}
