package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Wages {
    private Integer id;
    private BigDecimal basewage;
    private BigDecimal gradewage;
    private BigDecimal performance;
    private BigDecimal otherwage;
    private BigDecimal fiveinsurance;
    private BigDecimal tax;
    private BigDecimal otherpayment;
    private BigDecimal sumwage;
    private Integer userid;
    private String houseid;
    private String createtime;
    private String realname;
}


