package com.cw.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Curaccount {
    private Integer id;
    private Integer userid;
    private Integer paywayid;
    private String houseid;
    private String name;
    private BigDecimal money;
    private String realname;
    private String remark;


}


