package com.cw;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.event.TransactionalEventListener;

@Slf4j
@SpringBootApplication
@EnableTransactionManagement //开启事务管理
public class CwglApplication {

    public static void main(String[] args) {
        SpringApplication.run(CwglApplication.class, args);
        log.info("启动了........");
    }

}
